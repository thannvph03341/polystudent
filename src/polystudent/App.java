/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polystudent;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author nongthan
 */
public class App {
    
    ArrayList<SVPoly> sVPolys = null;
    Scanner scanner = null;

    public App() {
        scanner = new Scanner(System.in);
        this.sVPolys = new ArrayList<>();
    }
    
    public void initMenu() {
        System.out.println("Phần mềm quan lý thông tin sinh viên Poly");
        System.out.println(" ----------------------------------------- ");
        System.out.println("| 1. Nhập thông tin sinh viên             |");
        System.out.println("| 2. Danh sách sinh viên                  |");
//        System.out.println("| 3. Lấy sinh viên theo mã                |");
//        System.out.println("| 4. Sửa thông tin sinh viên              |");
//        System.out.println("| 5. Xoá sinh viên                        |");
        System.out.println("| 3. Thoát                                |");
        System.out.println(" ----------------------------------------- ");
        System.out.println("Vui lòng chọn từ 1 -> 3");
        System.out.print("Chọn: ");
        this.menu(scanner.nextInt());
    }
    
    private void menu (int c) {
        switch(c) {
            case 1:
              this.inputInfoStudent();
              break;
            case 2:
              this.showAllStudent();
              break;
            case 3:
              break;
        }
    }
    
    
    private void showAllStudent() {
        for (int i = 0; i < this.sVPolys.size(); i++) {
            SVPoly poly = this.sVPolys.get(i);
            System.out.println("-----------------------------");
            System.out.println("Họ tên: " + poly.getFullName());
            System.out.println("Tuổi: " + poly.getAge());
            System.out.println("Phone: " + poly.getPhone());
            System.out.println("Mã: " + poly.getCode());
            System.out.println("Địa chỉ: " + poly.getAdress());
            System.out.println("Sinh nhật: " + poly.getBirthday());
            System.out.println("-----------------------------");
        }
        this.initMenu();
    }
    
    private void inputInfoStudent() {
        SVPoly sv = new SVPoly();
        System.out.print("Họ: ");
        sv.setFirstName(scanner.next());
        System.out.print("Tên: ");
        sv.setLastName(scanner.next());
        System.out.print("Tuổi: ");
        sv.setAge(scanner.nextInt());
        System.out.print("Mã: ");
        sv.setCode(scanner.next());
        System.out.print("Sinh nhật: ");
        sv.setBirthday(scanner.next());
        System.out.print("Số điện thoại: ");
        sv.setPhone(scanner.next());
        System.out.print("Địa chỉ: ");
        sv.setAdress(scanner.next());
        this.sVPolys.add(sv);
        this.initMenu();
    }
}
