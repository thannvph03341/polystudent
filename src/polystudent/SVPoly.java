/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polystudent;

/**
 *
 * @author Hieu
 */
public class SVPoly {
    private String firstName = "";
    private String lastName = "";
    private int age = 0;
    private String code = null;
    private String birthday = "";
    private String adress = "";
    private String phone = "";
    
    public SVPoly() {}

    public void setAge(int age) {
        this.age = age;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    
    public String getFullName() {
        return this.firstName + " " + this.lastName;
    }

    public int getAge() {
        return age;
    }

    public String getBirthday() {
        return birthday;
    }

    public String getCode() {
        return code;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAdress() {
        return adress;
    }

    public String getPhone() {
        return phone;
    }
    

}
